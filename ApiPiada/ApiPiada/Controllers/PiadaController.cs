﻿using ApiPiada.Models;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;

namespace ApiPiada.Controllers
{
    public class PiadaController : ApiController
    {
        [System.Web.Http.Route("WebPiadas/ServicoPiada")]
        public JsonResult<Object> Get()
        {
            List<Piada> a = new Piada().listar();

            return Json((object)a);
        }

        [System.Web.Http.Route("WebPiadas/ServicoPiada/Categorias")]
        public JsonResult<Object> GetCategorias()
        {
            var a = new Categoria().listar();

            return Json((object)a);
        }

        [System.Web.Http.Route("WebPiadas/ServicoPiada")]
        public JsonResult<object> Get(string aleatoria)
        {
            List<Piada> a = new Piada().listar();
            if (aleatoria.ToUpper() == "SIM")
            {
                
                if (a.Count > 0)
                {
                    Random randNum = new Random();
                    return Json((object)new List<Piada>() { a[randNum.Next(a.Count)] });
                }

            }
            return Json((object)a);
        }

        [System.Web.Http.Route("WebPiadas/ServicoPiada/Pesquisa")]
        public JsonResult<object> GetPesquisa(string texto="", string categoria="0", string faixa = "")
        {
            string param = texto.Trim();
            int cat_id = 0;
            char faixa_etaria = ' ';

            if(categoria.Trim() != "0")
                int.TryParse(categoria, out cat_id);
            if(faixa.Trim() != "")
                char.TryParse(faixa, out faixa_etaria);

            List<Piada> piadas = new Piada().pesquisar(param, cat_id, faixa_etaria);

            return Json((object)piadas);
        }
    }
}
