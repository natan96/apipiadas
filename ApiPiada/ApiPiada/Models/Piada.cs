﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace ApiPiada.Models
{
    public class Piada
    {
        private int id;
        private string titulo;
        private string texto;
        private char faixaEtaria;
        private Categoria categoria;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        public string Texto
        {
            get { return texto; }
            set { texto = value; }
        }

        public string Titulo
        {
            get { return titulo; }
            set { titulo = value; }
        }

        public char FaixaEtaria
        {
            get { return faixaEtaria; }
            set { faixaEtaria = value; }
        }

        public Categoria Categoria
        {
            get { return categoria; }
            set { categoria = value; }
        }

        public List<Piada> TableToList(DataTable dt)
        {

            List<Piada> dados = new List<Piada>();
            if (dt != null && dt.Rows.Count > 0)
                dados = (from DataRow row in dt.Rows
                         select new Piada()
                         {
                             Id = int.Parse(row["pia_id"].ToString()),
                             Titulo = row["pia_titulo"].ToString(),
                             Texto = row["pia_texto"].ToString(),
                             FaixaEtaria = char.Parse(row["pia_faixa_etaria"].ToString()),
                             Categoria = new Categoria()
                             {
                                 Id = int.Parse(row["cat_id"].ToString()),
                                 Descricao = row["cat_descricao"].ToString(),
                             }

                         }).ToList();
            return dados;
        }

        public List<Piada> listar()
        {
            DataTable dt;
            using (BancoInstance b = new BancoInstance())
            {
                b.Banco.ExecuteQuery("select p.*,c.cat_descricao from Piada p inner join Categoria c on p.cat_id = c.cat_id", out dt);
            }
            return TableToList(dt);
        }

        public List<Piada> pesquisar(string texto, int cat_id, char faixa_etaria)
        {
            DataTable dt;
            using (BancoInstance b = new BancoInstance())
            {
                string sql = " SELECT p.*, c.cat_descricao"+
                             " FROM Piada p"+
                             " INNER JOIN Categoria c on c.cat_id = p.cat_id"+
                             " WHERE upper(pia_texto) LIKE @texto";
                if (cat_id > 0)
                {
                    sql += " AND p.cat_id = @cat_id";
                }

                if (faixa_etaria != ' ')
                {
                    sql += " AND p.pia_faixa_etaria = @faixa_etaria";
                }

                sql += " ORDER BY p.pia_titulo";

                if (cat_id > 0 && faixa_etaria != ' ')
                    b.Banco.ExecuteQuery(sql, out dt, "@texto", "%" + texto + "%", "@cat_id", cat_id, "@faixa_etaria", faixa_etaria);
                else if (faixa_etaria != ' ')
                    b.Banco.ExecuteQuery(sql, out dt, "@texto", "%" + texto + "%", "@faixa_etaria", faixa_etaria);
                else
                    b.Banco.ExecuteQuery(sql, out dt, "@texto", "%" + texto + "%", "@cat_id", cat_id);
            }
            return TableToList(dt);
        }
    }
}