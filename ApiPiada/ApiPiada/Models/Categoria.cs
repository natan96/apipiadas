﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace ApiPiada.Models
{
    public class Categoria
    {
        private int id;
        private string descricao;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        public string Descricao
        {
            get { return descricao; }
            set { descricao = value; }
        }

        public List<Categoria> TableToList(DataTable dt)
        {

            List<Categoria> dados = new List<Categoria>();
            if (dt != null && dt.Rows.Count > 0)
                dados = (from DataRow row in dt.Rows
                         select new Categoria()
                         {
                             Id = int.Parse(row["cat_id"].ToString()),
                             Descricao = row["cat_descricao"].ToString(),
                         }).ToList();
            return dados;
        }


        public List<Categoria> listar()
        {
            DataTable dt;
            using (BancoInstance b = new BancoInstance())
            {
                
                b.Banco.ExecuteQuery("select * from Categoria", out dt);
            }
            return TableToList(dt);
        }
    }
}